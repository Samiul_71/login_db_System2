<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <br>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#new" role="tab" data-toggle="tab" class="big">New User</a>
                        </li>

                        <li><a href="#user" role="tab" data-toggle="tab" class="big">I have account</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="new">
                            <br>
                            <form action="add.php" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-envelope"></i>
                                            <input class="form-control input-lg" placeholder="Full Name" type="text" name="title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-envelope"></i>
                                            <input class="form-control input-lg" placeholder="Email Address" type="text" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-lock"></i>
                                            <input class="form-control input-lg" placeholder="Password" type="password" name="password">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-lock"></i>
                                            <textarea rows="4" cols="50" placeholder="description" name="description"></textarea>
                                        </div>
                                    </div>


                                    <div class=" text-center">
                                        <button class="btn btn-primary">Registration</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>








                        <div class="tab-pane fade" id="user">
                            <br>
                            <form action="check.php" method="post">
                                <fieldset>

                                    <div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-envelope"></i>
                                            <input class="form-control input-lg" placeholder="Email Address" type="text" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="right-inner-addon">
                                            <i class="glyphicon glyphicon-lock"></i>
                                            <input class="form-control input-lg" placeholder="Password" type="password" name="password">
                                        </div>
                                    </div>
                                    <div class=" text-center">

                                        <button class="btn btn-primary">LOGIN</button>
                                    </div>

                                </fieldset>


                            </form>
                        </div>
                    </div>
                    <br>
                    <?php
                    if (isset($_SESSION['add'])) {
                        echo $_SESSION['add'];
                        unset($_SESSION['add']);
                    }
                    
                    if (isset($_SESSION['success'])) {
                        echo $_SESSION['success'];
                        unset($_SESSION['success']);
                        
                        
                    } else if (isset($_SESSION['unsuccess'])) {
                        echo $_SESSION['unsuccess'];
                        unset($_SESSION['unsuccess']);
                    }
                    ?>
                    <!-- Nav tabs -->

                    <div class="tab-content">

                        <div class="tab-pane fade text-center" id="bc">
                            <h2>$9<sup>,99</sup>/mo with <i class="fa fa-bitcoin"></i>
                            </h2>
                            <div class="alert alert-info fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <p class="text-left">
                                    Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio.
                                </p>
                            </div>
                            <a href="#" class="q text-center">What is BitCoin?</a><br>
                            <br>
                            <button class="btn btn-primary btn-lg btn-block">PAY $9<sup>,99</sup> USD WITH BITCOIN</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.js"></script>

    </body>
</html>